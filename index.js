function order(){
    // console.log("yes");
    var a = document.getElementById("txt-num1").value*1;
    var b = document.getElementById("txt-num2").value*1;
    var c = document.getElementById("txt-num3").value*1;
    // console.log({a,b,c});
    var result;

    if (a > b && a > c && b > c){
        result = c + " < " + b + " < " + a;
        // console.log(c,b,a);
    } else if (a > b && a > c && c > b){
        result = b + " < " + c + " < " + a;
        // console.log(b,c,a);
    } else if (b > a && b > c && a > c){
        result = c + " < " + a + " < " + b;
        // console.log(c,a,b);
    } else if (b > a && b > c && c > a){
        result = c + " < " + a + " < " + b;
        // console.log(a,c,b);
    } else if (c > b && c > a && a > b){
        result = b + " < " + a + " < " + c;
        // console.log(b,a,c);
    } else {
        result = a + " < " + b + " < " + c;
        // console.log(a,b,c);
    } 
    
    document.getElementById("showResult").innerHTML = result;
}

function sayHello(){
    var select = document.getElementById("txt-family-member");
    var value = select.options[select.selectedIndex].value;
    // console.log(value);
    var result;

    if (value == "D") {
        result = "Hello Dad";
    } else if (value == "M") {
        result = "Hello Mom";
    } else if (value == "B") {
        result = "Hello Brother";
    } else {
        result = "Hello Sister";
    }

    document.getElementById("showHello").innerHTML = result;
}

function countNum(){
    var num1 = document.getElementById("txt-num-1").value*1;
    var num2 = document.getElementById("txt-num-2").value*1;
    var num3 = document.getElementById("txt-num-3").value*1;
    var even_count = 0;
    var odd_count = 0;
    var num_arr = [num1,num2,num3];
    var result;

    for (var i = 0; i < num_arr.length; i++) {
        if(num_arr[i]%2 == 0) {
            even_count++;
        } else {
            odd_count++;
        }
    }
    result = "Number of even numbers: "+ even_count + " , " + "Number of odd number: " + odd_count;
    document.getElementById("showNumResult").innerHTML = result;
    
    console.log({result});
}

function triangle() {
    var edge1 = document.getElementById("txt-edge-1").value*1;
    var edge2 = document.getElementById("txt-edge-2").value*1;
    var edge3 = document.getElementById("txt-edge-3").value*1;
    var result;

    if (edge1 == edge2 && edge2 == edge3) {
        result = "Equilateral Triangle";
    } else if (edge1 == edge2 || edge2 == edge3 || edge1 == edge3) {
        result = "Isosceles Triangle"
    } else if (edge3*edge3 == (edge1*edge1) + (edge2*edge2)) {
        result = "Right Triangle";
    } else {
        result = "Another kind of Triangle";
    }

    document.getElementById("showTriangleResult").innerHTML = result;
}
